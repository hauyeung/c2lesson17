﻿namespace Arrays
{
    partial class Arrays
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.closeButton = new System.Windows.Forms.Button();
            this.weekdaysEnglishComboBox = new System.Windows.Forms.ComboBox();
            this.weekdaysSpanishComboBox = new System.Windows.Forms.ComboBox();
            this.weekdaysFrenchComboBox = new System.Windows.Forms.ComboBox();
            this.weekdaysAncientGreekComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Weekdays (English):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Weekdays (Spanish):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Weekdays (French):";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Weekdays (Ancient Greek):";
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(304, 191);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 8;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // weekdaysEnglishComboBox
            // 
            this.weekdaysEnglishComboBox.FormattingEnabled = true;
            this.weekdaysEnglishComboBox.Location = new System.Drawing.Point(226, 23);
            this.weekdaysEnglishComboBox.Name = "weekdaysEnglishComboBox";
            this.weekdaysEnglishComboBox.Size = new System.Drawing.Size(303, 21);
            this.weekdaysEnglishComboBox.TabIndex = 9;
            this.weekdaysEnglishComboBox.SelectedIndexChanged += new System.EventHandler(this.weekdaysEnglishComboBox_SelectedIndexChanged_1);
            // 
            // weekdaysSpanishComboBox
            // 
            this.weekdaysSpanishComboBox.FormattingEnabled = true;
            this.weekdaysSpanishComboBox.Location = new System.Drawing.Point(226, 58);
            this.weekdaysSpanishComboBox.Name = "weekdaysSpanishComboBox";
            this.weekdaysSpanishComboBox.Size = new System.Drawing.Size(303, 21);
            this.weekdaysSpanishComboBox.TabIndex = 10;
            this.weekdaysSpanishComboBox.SelectedIndexChanged += new System.EventHandler(this.weekdaysSpanishComboBox_SelectedIndexChanged_1);
            // 
            // weekdaysFrenchComboBox
            // 
            this.weekdaysFrenchComboBox.FormattingEnabled = true;
            this.weekdaysFrenchComboBox.Location = new System.Drawing.Point(226, 107);
            this.weekdaysFrenchComboBox.Name = "weekdaysFrenchComboBox";
            this.weekdaysFrenchComboBox.Size = new System.Drawing.Size(303, 21);
            this.weekdaysFrenchComboBox.TabIndex = 11;
            this.weekdaysFrenchComboBox.SelectedIndexChanged += new System.EventHandler(this.weekdaysFrenchComboBox_SelectedIndexChanged_1);
            // 
            // weekdaysAncientGreekComboBox
            // 
            this.weekdaysAncientGreekComboBox.FormattingEnabled = true;
            this.weekdaysAncientGreekComboBox.Location = new System.Drawing.Point(226, 144);
            this.weekdaysAncientGreekComboBox.Name = "weekdaysAncientGreekComboBox";
            this.weekdaysAncientGreekComboBox.Size = new System.Drawing.Size(303, 21);
            this.weekdaysAncientGreekComboBox.TabIndex = 12;
            this.weekdaysAncientGreekComboBox.SelectedIndexChanged += new System.EventHandler(this.weekdaysAncientGreekComboBox_SelectedIndexChanged_1);
            // 
            // Arrays
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 262);
            this.Controls.Add(this.weekdaysAncientGreekComboBox);
            this.Controls.Add(this.weekdaysFrenchComboBox);
            this.Controls.Add(this.weekdaysSpanishComboBox);
            this.Controls.Add(this.weekdaysEnglishComboBox);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Arrays";
            this.Text = "Arrays";
            this.Load += new System.EventHandler(this.Arrays_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.ComboBox weekdaysEnglishComboBox;
        private System.Windows.Forms.ComboBox weekdaysSpanishComboBox;
        private System.Windows.Forms.ComboBox weekdaysFrenchComboBox;
        private System.Windows.Forms.ComboBox weekdaysAncientGreekComboBox;
    }
}

