﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;


namespace csharp2_lesson17
{
    public class Alarms
    {
        public static List<Alarm> _alarms = new List<Alarm>();
        public bool Add(Alarm alarm)
        {
            _alarms.Add(alarm);
            Console.WriteLine(alarm._alarmTime.ToString());
            return true;
        }

        public Alarms()
        {
        }

        public int Count()
        {
            return _alarms.Count;
        }

        public bool Delete(DateTime alarm)
        {
            foreach (var a in _alarms)
            {
                if (_alarms.Equals(a))
                {
                    return _alarms.Remove(a);
                }
            }
            return false;
        }

        public Alarm GetAlarm(DateTime alarmTime)
        {
            foreach (var a in _alarms)
            {
                if (_alarms.Equals(a))
                {
                    return a;
                }
            }
            return null;
        }

        public Alarm GetAlarm(int i)
        {
            return _alarms[i];
        }

        public bool GetNextDue(DateTime currentDateTime, out Alarm alarmResult, bool due = false)
        {
            alarmResult = null;
            foreach (var a in _alarms)
            {
                if (a._alarmTime.Equals(currentDateTime))
                {
                    return true;                    
               }
            }        
            return false;
        }

        public void LoadAlarmsFromFile(string fileName)
        {
            string exeFolder = Directory.GetCurrentDirectory();

            System.IO.StreamReader file = new System.IO.StreamReader("c:\\temp\\"+fileName);
            file.Read();
            file.Close();
        }

        public void SaveAlarmsToFile(string fileName)
        {
            string exeFolder = Directory.GetCurrentDirectory();
            string path= exeFolder + "\\alarms.txt";
            

            using (StreamWriter s = File.AppendText(exeFolder +"\\"+ fileName))
            {
                foreach (var alarm in _alarms)
                {
                    string enable = "";
                    if (alarm._enabled == true)
                    {
                        enable = " Enabled";
                    }
                    else
                    {
                        enable = " Disabled";
                    }
                    s.WriteLine(alarm._alarmTime.ToString() + " - " + enable + "\n");
                } 
            }
        }

        public bool Search(DateTime targetAlarm)
        {
            foreach (var a in _alarms)
            {
                if (a._alarmTime.Equals(targetAlarm))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
