﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace csharp2_lesson17
{
    public partial class AlarmForm : Form
    {
        public static Alarms alarms;
        MainForm m = new MainForm();
        private string newitem;
        public string Newitem
        {
            set
            {
                newitem = value;
            }
            get
            {
                return newitem;
            }
        }
        public AlarmForm()
        {
            InitializeComponent();
            alarms = new Alarms();
        }

        private void categoryComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void okButton_Click(object sender, EventArgs e)
        {
            
            if (m.alarmListBox.SelectedItem == null)
            {
                string cat = categoryComboBox.Text;
                DateTime alarmtime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(hourUpDown.Text), Convert.ToInt32(minuteUpDown.Text), 1);

                string[] alarmcats = new string[100];
                int count = categoryComboBox.Items.Count;
                for (int x = 0; x < count; x++)
                {
                    alarmcats[x] = Convert.ToString(categoryComboBox.Items[x]);
                }

                Alarm a = new Alarm(cat, alarmtime, true, enabledCheckBox.Checked, alarmcats);
                string enable = "";
                if (enabledCheckBox.Checked == true)
                {
                    enable = "Enabled";
                }
                else
                {
                    enable = "Disabled";
                }          
                newitem = a._alarmTime.ToString()+ " " + enable ;
                if (alarms.Search(alarmtime)==true)
                {
                    errorLabel.Text = "Existing Alarm";
                }
                else
                {
                    alarms.Add(a);
                    alarms.SaveAlarmsToFile("alarms.txt");
                    MessageBox.Show("Alarm added");
                    this.DialogResult = DialogResult.OK;
                }

            }
            else
            {
                string cat = categoryComboBox.Text;
                DateTime alarmtime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(hourUpDown.Text), Convert.ToInt32(minuteUpDown.Text), 1);

                string[] alarmcats = new string[100];
                int count = categoryComboBox.Items.Count;
                Alarm a = new Alarm(cat, alarmtime, true, enabledCheckBox.Checked, alarmcats);
                alarms.Add(a);
                alarms.SaveAlarmsToFile("alarms.txt");
                m.alarmListBox.Items.Remove(m.alarmListBox.SelectedItem);
                newitem = hourUpDown.Text + ":" + minuteUpDown.Text + " Enabled";
                this.DialogResult = DialogResult.OK;
            }

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }


    }
}
