﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Arrays
{
    public partial class Arrays : Form
    {
        public Arrays()
        {
            InitializeComponent();

            // Set up weekday ComboBox controls.
            populateWeekdayComboBoxes();
        }

        private void populateWeekdayComboBoxes()
        {
            // Declare weekday arrays.
            string[] daysOfWeekEnglish = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
            string[] daysOfWeekSpanish = { "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" };
            string[] daysOfWeekFrench = { "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi" };
            string[] daysOfWeekAncientGreek = { "ἡμέρα Ἡλίου (heméra Helíou)", 
                                                "ἡμέρα Σελήνης (heméra Selénes)", 
                                                "ἡμέρα Ἄρεως (heméra Áreos)", 
                                                "ἡμέρα Ἕρμου (heméra Hérmou)", 
                                                "ἡμέρα Διός (heméra Diós)", 
                                                "ἡμέρα Ἀφροδίτης (heméra Aphrodítes)", 
                                                "ἡμέρα Κρόνου (heméra Krónou)" };

            // Populate each ComboBox.
            populateWeekdayComboBox(weekdaysEnglishComboBox, daysOfWeekEnglish);
            populateWeekdayComboBox(weekdaysSpanishComboBox, daysOfWeekSpanish);
            populateWeekdayComboBox(weekdaysFrenchComboBox, daysOfWeekFrench);
            populateWeekdayComboBox(weekdaysAncientGreekComboBox, daysOfWeekAncientGreek);

            // Set default to the first entry in each ComboBox (0-based index).
            setWeekDayComboBoxes(0);
        }

        private void populateWeekdayComboBox(ComboBox comboBox, string[] daysOfWeek)
        {
            // Make sure we have a valid array.
            if (daysOfWeek != null)
            {
                // Clear, then add the days of the week to the ComboBox.
                comboBox.Items.Clear();
                for (int i = 0; i < daysOfWeek.Length; i++)
                {
                    comboBox.Items.Add(daysOfWeek[i].ToString());
                }
            }
        }

        private void setWeekDayComboBoxes(int weekDay)
        {
            // Set each ComboBox SelectedIndex.
            weekdaysEnglishComboBox.SelectedIndex = weekDay;
            weekdaysSpanishComboBox.SelectedIndex = weekDay;
            weekdaysFrenchComboBox.SelectedIndex = weekDay;
            weekdaysAncientGreekComboBox.SelectedIndex = weekDay;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void weekdaysEnglishComboBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            // SelectedIndex changed, so change all of the rest of the ComboBox controls.
            setWeekDayComboBoxes(weekdaysEnglishComboBox.SelectedIndex);
        }

        private void weekdaysSpanishComboBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            // SelectedIndex changed, so change all of the rest of the ComboBox controls.
            setWeekDayComboBoxes(weekdaysSpanishComboBox.SelectedIndex);
        }

        private void weekdaysFrenchComboBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            // SelectedIndex changed, so change all of the rest of the ComboBox controls.
            setWeekDayComboBoxes(weekdaysFrenchComboBox.SelectedIndex);
        }

        private void Arrays_Load(object sender, EventArgs e)
        {

        }

        private void weekdaysAncientGreekComboBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            // SelectedIndex changed, so change all of the rest of the ComboBox controls.
            setWeekDayComboBoxes(weekdaysAncientGreekComboBox.SelectedIndex);
        }
    }
}

