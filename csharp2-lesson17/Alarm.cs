﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;


namespace csharp2_lesson17
{
    public class Alarm
    {
        public string _alarmCategory;
        public DateTime _alarmTime;
        public bool _due;
        public bool _enabled;
        public string[] AlarmCategories;

        string AlarmCategory { get; set; }
        DateTime AlarmTime {get; set;}
        bool Enabled { get; set; }

        public Alarm(string alarmcat, DateTime alarmtime, bool due, bool enabled, string[] alarmcats)
        {
            _alarmCategory = alarmcat;
            _alarmTime = alarmtime;
            _due = due;
            _enabled = enabled;
            AlarmCategories = alarmcats;
        }

        public override string ToString()
        {
            return Convert.ToString(_alarmTime);
        }

    }
}
