﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace csharp2_lesson17
{
    public partial class MainForm : Form
    {

        public MainForm()
        {
            InitializeComponent();           
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            using (AlarmForm af = new AlarmForm())
            {
                if (af.ShowDialog() == DialogResult.OK)
                {
                    // result OK - get new item
                    this.alarmListBox.Items.Add(af.Newitem);
                }
            }


        }

        private void editButton_Click(object sender, EventArgs e)
        {
            using (AlarmForm af = new AlarmForm())
            {
                if (af.ShowDialog() == DialogResult.OK)
                {
                    // result OK - get new item
                    this.alarmListBox.Items.Remove(this.alarmListBox.SelectedItem);
                    this.alarmListBox.Items.Add(af.Newitem);
                }
            };
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (alarmListBox.Items != null)
            {
                if (MessageBox.Show("Delete " + alarmListBox.SelectedItem.ToString() + "alarm?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    alarmListBox.Items.Remove(alarmListBox.SelectedItem);
                }

            }

        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void alarmListBox_SelectedIndexChanged(object sender, EventArgs e)
        {




        }

        private void timer_Tick(object sender, EventArgs e)
        {            
            this.Refresh();          
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            timelabel.Text = DateTime.Now.ToString();            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Alarms a = new Alarms();
            foreach (var x in Alarms._alarms)
            {
                Console.WriteLine(x._alarmTime.ToString());
                if ((x._alarmTime - DateTime.Now).TotalSeconds < 0 && x._enabled)
                {                    
                    x._enabled = false;
                    MessageBox.Show("Alarm "+x._alarmTime.ToString());                                               
                    break;                                        
                }                
            }
        }

    }
}

